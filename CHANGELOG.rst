=========
ChangeLog
=========


v0.1.3
======

* Added `xform` helper method
* Added hierarchical key specification (`tree`) in `pick` and `omit`


v0.1.2
======

* Removed `distribute` dependency
* Added `split` keyword to `tolist`
* Improved `default` handling of `tobool`


v0.1.1
======

* First tagged release
